/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var click = exports.click = function click(el) {
    console.log('el');
    console.log(el);

    fireEvt("keydown", el);
    fireEvt("keypress", el);
    fireEvt("keyup", el);
};

function fireEvt(type, element) {
    var customEvent;
    var type = type;
    var bubbles = true;
    var cancelable = true;
    var view = window;
    var ctrlKey = false;
    var altKey = false;
    var shiftKey = false;
    var metaKey = false;
    var keyCode = 68;
    var charCode = 68;

    try {

        //try to create key event
        customEvent = document.createEvent("KeyEvents");

        /*
         * Interesting problem: Firefox implemented a non-standard
         * version of initKeyEvent() based on DOM Level 2 specs.
         * Key event was removed from DOM Level 2 and re-introduced
         * in DOM Level 3 with a different interface. Firefox is the
         * only browser with any implementation of Key Events, so for
         * now, assume it's Firefox if the above line doesn't error.
         */
        //TODO: Decipher between Firefox's implementation and a correct one.
        customEvent.initKeyEvent(type, bubbles, cancelable, view, ctrlKey, altKey, shiftKey, metaKey, keyCode, charCode);
    } catch (ex /*:Error*/) {

        /*
         * If it got here, that means key events aren't officially supported.
         * Safari/WebKit is a real problem now. WebKit 522 won't let you
         * set keyCode, charCode, or other properties if you use a
         * UIEvent, so we first must try to create a generic event. The
         * fun part is that this will throw an error on Safari 2.x. The
         * end result is that we need another try...catch statement just to
         * deal with this mess.
         */
        try {

            //try to create generic event - will fail in Safari 2.x
            customEvent = document.createEvent("Events");
        } catch (uierror /*:Error*/) {

            //the above failed, so create a UIEvent for Safari 2.x
            customEvent = document.createEvent("UIEvents");
        } finally {

            customEvent.initEvent(type, bubbles, cancelable);

            //initialize
            customEvent.view = view;
            customEvent.altKey = altKey;
            customEvent.ctrlKey = ctrlKey;
            customEvent.shiftKey = shiftKey;
            customEvent.metaKey = metaKey;
            customEvent.keyCode = keyCode;
            customEvent.charCode = charCode;
        }
    }

    //fire the event
    element.focus();
    element.dispatchEvent(customEvent);
}

/***/ })
/******/ ]);