/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var click = exports.click = function click(el) {
    console.log('el');
    console.log(el);

    fireEvt("keydown", el);
    fireEvt("keypress", el);
    fireEvt("keyup", el);
};

function fireEvt(type, element) {
    var customEvent;
    var type = type;
    var bubbles = true;
    var cancelable = true;
    var view = window;
    var ctrlKey = false;
    var altKey = false;
    var shiftKey = false;
    var metaKey = false;
    var keyCode = 68;
    var charCode = 68;

    try {

        //try to create key event
        customEvent = document.createEvent("KeyEvents");

        /*
         * Interesting problem: Firefox implemented a non-standard
         * version of initKeyEvent() based on DOM Level 2 specs.
         * Key event was removed from DOM Level 2 and re-introduced
         * in DOM Level 3 with a different interface. Firefox is the
         * only browser with any implementation of Key Events, so for
         * now, assume it's Firefox if the above line doesn't error.
         */
        //TODO: Decipher between Firefox's implementation and a correct one.
        customEvent.initKeyEvent(type, bubbles, cancelable, view, ctrlKey, altKey, shiftKey, metaKey, keyCode, charCode);
    } catch (ex /*:Error*/) {

        /*
         * If it got here, that means key events aren't officially supported.
         * Safari/WebKit is a real problem now. WebKit 522 won't let you
         * set keyCode, charCode, or other properties if you use a
         * UIEvent, so we first must try to create a generic event. The
         * fun part is that this will throw an error on Safari 2.x. The
         * end result is that we need another try...catch statement just to
         * deal with this mess.
         */
        try {

            //try to create generic event - will fail in Safari 2.x
            customEvent = document.createEvent("Events");
        } catch (uierror /*:Error*/) {

            //the above failed, so create a UIEvent for Safari 2.x
            customEvent = document.createEvent("UIEvents");
        } finally {

            customEvent.initEvent(type, bubbles, cancelable);

            //initialize
            customEvent.view = view;
            customEvent.altKey = altKey;
            customEvent.ctrlKey = ctrlKey;
            customEvent.shiftKey = shiftKey;
            customEvent.metaKey = metaKey;
            customEvent.keyCode = keyCode;
            customEvent.charCode = charCode;
        }
    }

    //fire the event
    element.focus();
    element.dispatchEvent(customEvent);
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var GET_LOGIN_INFO = exports.GET_LOGIN_INFO = "Content:GetLoginInfo";
var SAVE_SUBMIT_INFO = exports.SAVE_SUBMIT_INFO = "Content:SaveSubmitInfo";

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _actionNames = __webpack_require__(1);

var _clicker = __webpack_require__(0);

var MAX_INPUTS_FOR_LOGIN = 4;
// import {clicker} from './clicker';


var PASS_FIELD_RULES = ["input[type='password']", "input#password"];
var LOGIN_FIELD_RULES = ["input[type='text']", "input:not([type])", "input[type='email']"];

var getVisibleForms = function getVisibleForms() {
    return $("form").filter(":visible");
};
var getField = function getField(rules, form) {
    var result = void 0;
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = rules[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var rule = _step.value;

            result = $(rule, form);

            if (result.lenght > 0) {
                return result;
            }
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }
};

var getVisibleInputs = function getVisibleInputs(form) {
    return $("input:not([type=hidden])", form).filter(":visible");
};

var isLoginForm = function isLoginForm(login, pass, form) {
    return pass && login && getVisibleInputs(form).length <= MAX_INPUTS_FOR_LOGIN;
};

var getLoginForm = function getLoginForm() {
    var forms = getVisibleForms();

    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
        for (var _iterator2 = forms[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var form = _step2.value;

            var login = getField(LOGIN_FIELD_RULES, form);
            var pass = getField(PASS_FIELD_RULES, form);

            if (isLoginForm(login, pass, form)) {
                return {
                    login: login,
                    pass: pass,
                    form: form
                };
            }
        }
    } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                _iterator2.return();
            }
        } finally {
            if (_didIteratorError2) {
                throw _iteratorError2;
            }
        }
    }

    return null;
};

var getDomain = function getDomain() {
    return document.location.hostname;
};

var submitForm = function submitForm(lf, lp, domain) {
    lf.login.val(lp.username);
    lf.pass.val(lp.password);

    chrome.extension.sendRequest({ action: _actionNames.SAVE_SUBMIT_INFO, hash: lp.hash, domain: domain }, function () {
        lf.form.submit();
    });
};

var domain = getDomain();

console.log('Sending message...');

chrome.runtime.sendMessage({
    action: _actionNames.GET_LOGIN_INFO,
    url: document.location.toString(),
    domain: domain
}, function (msg) {
    console.log('msg: ' + JSON.stringify(msg));

    if (msg.auth_flow.type == 'basic') {
        var lf = getLoginForm();

        if (lf) {
            if (msg.logins.length > 1) {
                // It is possible that a user can have more than one account for the same site.
                // We need to provide him an ability to choose the necessary acc.
            } else if (msg.logins.length == 1) {
                var lp = msg.logins[0];
                //TODO: explain more

                // We need to avoid cyclic form resubmission in the case when a user has provided incorrect credentials.
                // At the same time we should allow a user to sign in again if he has updated his login/pass in the system
                // and now it will be correct
                var canSubmit = msg.last_domain != domain || msg.last_hash != lp.hash;
                if (canSubmit) {
                    submitForm(lf, lp, domain);
                }
            }
        }
    } else if (msg.auth_flow.type == 'custom') {
        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
            for (var _iterator3 = msg.auth_flow.steps[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var step = _step3.value;

                var val = step.value == '${username}' ? msg.logins[0].username : step.value;

                console.log('val: ' + val);

                $(step.selector).val(val);

                if (step.next_action == 'click') {
                    console.log('$(step.next)');
                    console.log($(step.next));
                    console.log($(step.next)[0]);

                    var el = document.getElementById('identifierNext');
                    // click($(step.next)[0]);
                    (0, _clicker.click)(el);
                }
            }
        } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                }
            } finally {
                if (_didIteratorError3) {
                    throw _iteratorError3;
                }
            }
        }
    }
});

/***/ })
/******/ ]);