// Note that this config is a bit different to the usual config.js in that it
// includes all envs. This is done because the use-case of this app is quite
// different to the usual Node app which is "runnable".

// If you're editing RECRUITER_BROWSER_EXTENSION_APP_HOST, make sure to also
// update `manifest.json`!

module.exports = {
  local: {
  },
  dev: {
  },
  production: {
  }
};
