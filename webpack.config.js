const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const { EnvironmentPlugin } = require("webpack");
const config = require("./config");

module.exports = env => ({
  entry: {
    background: "./src/background.js",
    content: "./src/content.js",
    clicker: "./src/clicker.js"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "dist", env.target),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{ loader: "babel-loader", options: { presets: ["env"] } }],
      },
    ],
  },
  plugins: [
    new CopyPlugin([
      { from: "src/manifest.json" },
      /* { from: "src/icons", to: "icons" }, */
    ]),
    new EnvironmentPlugin(config[env.target]),
  ],
});
