import {GET_LOGIN_INFO, SAVE_SUBMIT_INFO} from './action-names';
// import {clicker} from './clicker';
import {click} from './clicker';

const MAX_INPUTS_FOR_LOGIN = 4;

const PASS_FIELD_RULES = ["input[type='password']", "input#password"];
const LOGIN_FIELD_RULES = ["input[type='text']", "input:not([type])", "input[type='email']"];

const getVisibleForms = () => $("form").filter(":visible");
const getField = (rules, form) => {
    let result;
    for (let rule of rules) {
        result = $(rule, form);

        if (result.lenght > 0) {
            return result;
        }
    }
}

const getVisibleInputs = (form) => $("input:not([type=hidden])", form).filter(":visible");

const isLoginForm = (login, pass, form) => pass && login && getVisibleInputs(form).length <= MAX_INPUTS_FOR_LOGIN;

const getLoginForm = () => {
    const forms = getVisibleForms();

    for (let form of forms) {
        const login = getField(LOGIN_FIELD_RULES, form);
        const pass = getField(PASS_FIELD_RULES, form);

        if (isLoginForm(login, pass, form)) {
            return {
                login: login,
                pass: pass,
                form: form
            };
        }
    }

    return null;
};

const getDomain = () => document.location.hostname;

const submitForm = (lf, lp, domain) => {
    lf.login.val(lp.username);
    lf.pass.val(lp.password);

    chrome.extension.sendRequest({action: SAVE_SUBMIT_INFO, hash: lp.hash, domain: domain}, () => {
        lf.form.submit();
    });
}

const domain = getDomain();

console.log('Sending message...');

chrome.runtime.sendMessage({
    action: GET_LOGIN_INFO,
    url: document.location.toString(),
    domain: domain
}, (msg) => {
    console.log('msg: ' + JSON.stringify(msg));

    if (msg.auth_flow.type == 'basic') {
        const lf = getLoginForm();

        if (lf) {
            if (msg.logins.length > 1) {
                // It is possible that a user can have more than one account for the same site.
                // We need to provide him an ability to choose the necessary acc.
            } else if (msg.logins.length == 1) {
                const lp = msg.logins[0];
                //TODO: explain more

                // We need to avoid cyclic form resubmission in the case when a user has provided incorrect credentials.
                // At the same time we should allow a user to sign in again if he has updated his login/pass in the system
                // and now it will be correct
                const canSubmit = msg.last_domain != domain || msg.last_hash != lp.hash;
                if (canSubmit) {
                    submitForm(lf, lp, domain);
                }
            }
        }
    } else if(msg.auth_flow.type == 'custom') {
        for (let step of msg.auth_flow.steps) {
            let val = step.value == '${username}' ? msg.logins[0].username : step.value;

            console.log('val: ' + val);

            $(step.selector).val(val);
            
            if (step.next_action == 'click') {
                console.log('$(step.next)');
                console.log($(step.next));
                console.log($(step.next)[0]);

                let el = document.getElementById('identifierNext');
                // click($(step.next)[0]);
                click(el);
            }
        }
    }
});

