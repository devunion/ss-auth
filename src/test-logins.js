export const TEST_LOGINS = {
    'accounts.google.com': {
        logins: [
            {username: 'devunion@gmail.com', password: 'test'}  
        ],
        auth_flow: {
            type: 'custom',
            steps: [{
                value: '${username}',
                selector: 'input[type="email"]',
                next: '#identifierNext',
                next_action: 'click'
            }]
        }
    }
}; 