import md5 from './lib/md5';
import * as clicker from './clicker';

import {GET_LOGIN_INFO, SAVE_SUBMIT_INFO} from "./action-names";
import {TEST_LOGINS} from "./test-logins";

const domain2li = {};

const saveSubmitInfo = (domain, hash, callback) => {
    var li = domain2li[domain];
    
    if (li && li.logins.length > 0) {
        var now = new Date().getTime();
        if (li.last_login_time && li.last_hash == hash && now - li.last_login_time < 60*1000) {
            return;
        }

        li.last_hash = hash;
        li.last_login_time = now;
    }

    sendResponse({});
};

const addHash = (lp) => Object.assign(lp, {hash : md5(`${lp.login}@${lp.password}`)});

function getLoginInfo(url, domain, callback) {
    const config = TEST_LOGINS[domain];
    console.log('config: ' + JSON.stringify(config));

    if (!config) {
        return;
    }

    const result = Object.assign(config, {
    });

    // config.logins = config.logins.map(addHash);

    const oldLI = domain2li[domain];
    
    if (oldLI && result.logins.length > 0) {
        //TODO: why we do config[0] here???
        // what if we've more than one login pair in the list???
        
        if (oldLI.last_hash != result.logins[0].hash) {
            result.last_login_time = null;
        } else {
            result.last_hash = oldLI.last_hash;
            result.last_login_time = oldLI.last_login_time;
        }
    }
    domain2li[domain] = result;

    console.log('result: ' + JSON.stringify(result));
    callback(result);
}

const init = () => {
    console.log('init');

    chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
        console.log('message: ' + JSON.stringify(message));

        if (!sender.tab) {
            return;
        }

        console.log('message.action: ' + message.action);
        console.log('GET_LOGIN_INFO: ' + GET_LOGIN_INFO);

        switch (message.action) {
            case GET_LOGIN_INFO:
                getLoginInfo(message, message.domain, sendResponse);
                break;

            case SAVE_SUBMIT_INFO:
                saveSubmitInfo(message.domain, message.hash, sendResponse)
                break;
        }

        return false;
    });
}

$.ajaxSetup({cache: false});
init();

