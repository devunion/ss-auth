var pageMod = require("sdk/page-mod");
var self = require("sdk/self");
var data = self.data;
var Request = require("sdk/request").Request;
var ss = require("sdk/simple-storage");
var tabs = require("sdk/tabs");

const crypto = require('crypto');
const protocol = require('protocol/index');

var domain2li = {};
var domain2frames = {};

var URL_LOGINS = "http://localhost/plugin/getLogins";

const speedfoxProtocolHandler = protocol.protocol('favsync', {
    onRequest: function (request, response) {
        if (request.uri.indexOf("favsync://") == 0) {
            response.uri = self.data.url() + request.uri.replace("favsync://", "");
        }
    }
});

speedfoxProtocolHandler.register();

function messageHandler(request, worker) {
    function sendResponse(resp) {
        console.log("posting response: " + JSON.stringify(resp));
        return worker.postMessage(resp);
    }

    console.log("request: " + JSON.stringify(request) + ", worker: " + worker);

    if (request.name == "get_login_info") {
        if (!worker && request.src == "filler") {
            var frames = domain2frames[request.domain];
            if (!frames) {
                frames = [];
            }

            frames.push({url: request.url, lf: request.lf});

            domain2frames[request.domain] = frames;

            getLoginInfo(request.url, request.domain, request.src, function (msg) {
                tab2mainWorker[tabs.activeTab].postMessage(msg);
            });
            return;
        }

        getLoginInfo(request.url, request.domain, request.src, sendResponse);
    } else if (request.name == "insert_lp" ||
        request.name == "use_pass" ||
        request.name == "cancel_use_pass") {

        // We're receiving above mesages from favync:// pages.
        // Need to send them to top page worker.
        tab2mainWorker[tabs.activeTab].postMessage(request);
    } else if (request.name == "save_hash_and_last_domain") {
        var li = domain2li[request.domain];
        if (li && li.logins.length > 0) {
            var now = new Date().getTime();
            if (li.last_login_time && li.last_hash == request.hash && now - li.last_login_time < 60*1000) {
                console.log("Don't log in. 1 minute was not passed : " + (now - li.last_login_time));
                return;
            }

            li.last_hash = request.hash;
            li.last_login_time = now;
        }

        worker.postMessage({name: "save_hash_and_last_domain_response"});
    } else if (request.name == "cleanup_last_domain") {
        last_domain = null;

        worker.postMessage({name: "get_cleanup_last_domain_response"});
    } else if (request.name == "update_login_time") {
        delete domain2frames[request.domain];

        var li = domain2li[request.domain];
        console.log("li: " + li);

        if (li) {
            // li.last_hash is already updated.
            li.last_login_time = new Date().getTime();
        }
    } else if (request.name == "save_prop") {
        ss.storage[request.prop_name] = request.prop_val;
    } else if (request.name == "get_prop") {
        sendResponse({name: "get_prop_response", prop_val: ss.storage[request.prop_name]});
    }

}

var tab2mainWorker = {};
pageMod.PageMod({
    include: "*",
    contentScriptFile: ["js/jquery-1.8.2.min.js", "js/filler.js"].map(data.url),
    contentScriptWhen: "end",
    attachTo: ["top", "frame"],
    onAttach: function (worker) {
        tab2mainWorker[worker.tab] = worker;

        worker.on("message", function (msg) {
            messageHandler(msg, worker);
        });
        worker.on('detach', function (d) {
            delete tab2mainWorker[worker.tab];
        });
    }
});
pageMod.PageMod({
    include: "favsync://popup/passgen.html",
    contentScriptFile: ["js/jquery-1.8.2.min.js", "popup/passgen.js"].map(data.url),
    attachTo: ["frame"],
    onAttach: function (worker) {
        worker.on("message", function (msg) {
            messageHandler(msg, worker);
        });
    }
});
pageMod.PageMod({
    include: "favsync://popup/popup.html*",
    contentScriptFile: ["js/jquery-1.8.2.min.js", "popup/popup.js"].map(data.url),
    attachTo: ["top", "frame"],
    onAttach: function (worker) {
        worker.on("message", function (msg) {
            messageHandler(msg, worker);
        });
    }
});

function wasSuccessfullyLoggedIn(url, domain) {
    var frames = domain2frames[domain];

    if (!frames) {
        return false;
    }

    for (var i = 0; i < frames.length; i++) {
        var f = frames[i];

        if (f.url == url && !f.lf) {
            return false;
        }
    }

    return true;
}

function getLoginInfo(url, domain, src, callback) {
    if (src == "popup") {
        var li = domain2li[domain];
        if (li) {
            callback(li);
        }
        return;
    }

    Request({
        url: URL_LOGINS,
        headers: {'Cache-control': 'no-cache'},
        content: {code: "67f5bcd089dbd58e9e2f5fa3e29b027a", link: url},
        onComplete: function (r) {
            if (!r.json) {
                return;
            }

            var result = {
                name: "get_login_info_response",
                last_login_time: null,
                last_hash: null,
                logins: r.json.li.lp.map(function (lp) {
                        var l = lp.username;
                        var a = lp.action;
                        var p = crypto.decode(lp.password);
                        return {username: l, password: p, action : a, hash: crypto.md5(l + p)};
                    }
                )
            };

            var oldLI = domain2li[domain];
            if (oldLI && result.logins.length > 0) {
                if (oldLI.last_hash != result.logins[0].hash) {
                    result.last_login_time = null;
                } else {
                    result.last_hash = oldLI.last_hash;
                    result.last_login_time = oldLI.last_login_time;
                }
            }

            result.success = wasSuccessfullyLoggedIn(url, domain);

            callback(result);

            domain2li[domain] = result;
        }
    }).post();
}