$(document).ready(function(){
    var domain = document.location.toString().split("domain=")[1];

    self.on("message", function (msg) {
        if (msg.name == "get_login_info_response") {
            for (var i=0; i < msg.logins.length; i++) {
                var lp = msg.logins[i];
                $("#container").append(getLPBox(lp));
            }
        }
    });

    self.postMessage({name: "get_login_info", src : "popup", domain: domain});
});

function getLPBox(lp) {
    return $("<div class='lp-box' login='" + lp.username + "'>Login: " + lp.username + "</div>").click(function(){
        var login = $(this).attr("login");
        $(this).removeAttr("login");

        self.postMessage({name: "insert_lp", username : login});
    });
}
