function generatePass() {
    $("#pass").val(randomString(parseInt($("#length").val()), '#aA'));
}

function randomString(length, chars) {
    var mask = '';
    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.indexOf('#') > -1) mask += '0123456789';
    if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
    var result = '';
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    return result;
}

$(document).ready(function(){
    var domain = document.location.toString().split("domain=")[1];

    self.on("message", function (msg) {
        if (msg.name == "get_prop_response") {
            if (msg.prop_val) {
                $("#length").val(msg.prop_val);
                generatePass();
            }

        }
    });


    self.postMessage({name: "get_prop", prop_name : "pass_length"});

    $("#generate").click(generatePass);

    $("#length").change(function(){
        self.postMessage({name: "save_prop", prop_name: "pass_length", prop_val : $("#length").val()});

        generatePass();
    });

    $("#usePassword").click(function(){
        self.postMessage({name: "use_pass", pass: $("#pass").val()});

    });

    $("#cancel").click(function(){
        self.postMessage({name: "cancel_use_pass"});
    });
});
