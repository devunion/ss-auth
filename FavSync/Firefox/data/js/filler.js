const ACTION_AUTO_LOGIN = 0;
const ACTION_AUTO_FILL = 1;

self.on("message", function (request) {
    console.log("message: " + JSON.stringify(request));

    if (request.name == "get_login_info_response") {
        handleLoginInfo(request);
    } else if (request.name == "save_hash_and_last_domain_response") {
        lf.form.submit();
    } else if (request.name == "insert_lp") {
        removePopup();

        for (var i = 0; i < logins.length; i++) {
            if (request.username == logins[i].username) {
                console.log("1 lf: " + lf);
                insertLogin(logins[i], domain);
            }
        }
    } else if (request.name == "use_pass") {
        if (sf) {
            sf.pass.each(function () {
                $(this).val(request.pass);

                fireEvt("keydown", this);
                fireEvt("keypress", this);
                fireEvt("keyup", this);
            });
        }

        removePassGenPopup();
    } else if (request.name == "cancel_use_pass") {
        removePassGenPopup();
    }
});

const MAX_INPUTS_FOR_LOGIN = 4;

var logins, lf = null;
var isPopupVisible = false;
var domain = document.location.hostname;


var sf = getSignUpForm();
console.log("sf: " + sf);
if (sf) {
    addPassGenPopup(sf.pass);
}

self.postMessage({name: "get_login_info", url: document.location.toString(), src: "filler", domain: domain});

function handleLoginInfo(msg) {

    addPopupCloseHandler();

    lf = getLoginForm();
    console.log("lf: " + lf);
    if (lf) {
        if (msg.logins.length > 1) {
            logins = msg.logins;

            var lp = chooseLoginInfo(msg.logins);
            if (lp) {
                var canSubmit = msg.last_domain != domain || msg.last_hash != lp.hash;
                if (canSubmit) {
                    insertLogin(lp, domain);
                }
            } else {
                addPopup(lf.pass, msg);
            }
        } else if (msg.logins.length == 1) {
            var lp = msg.logins[0];

            var canSubmit = msg.last_domain != domain || msg.last_hash != lp.hash;
            if (canSubmit) {
                insertLogin(lp, domain);
            }
        }
    } else {
        //We've logged in successfully.
        console.log("msg.success: " + msg.success);
        if (msg.success) {
            self.postMessage({name: "update_login_time", domain : domain});
        }
    }
}

function chooseLoginInfo(logins) {
    var autologin, autofill = false;
    for (var i = 0; i < logins.length; i++) {
        var l = logins[i];

        if (l.action == ACTION_AUTO_LOGIN) {
            if (autologin) {
                return null;
            }

            autologin = l;
        }
        if (autofill && l.action == ACTION_AUTO_FILL) {
            if (autofill) {
                return null;
            }

            autofill = l;
        }
    }

    if (autologin) {
        return autologin;
    }
    if (autofill) {
        return autofill;
    }

    return null;
}

function insertLogin(lp, domain) {
    if (lp.action == ACTION_AUTO_LOGIN || lp.action == ACTION_AUTO_FILL) {
        lf.login.val(lp.username);
        lf.pass.val(lp.password);
    }

    if (lp.action == ACTION_AUTO_LOGIN) {
        self.postMessage({name: "save_hash_and_last_domain", hash: lp.hash, domain: domain});
    }
}

function addPopupCloseHandler() {
    $(document).click(function () {
        if (isPopupVisible) {
            removePopup();
            removePassGenPopup();
        }
    });
}

function addPopup(pass, msg) {
    pass.attr("style", "background-image:url(favsync://img/asterisk.png" + ");background-attachment:scroll;background-position: 100% 50%;background-repeat: no-repeat no-repeat").
        click(function (e) {
            if (isPopupVisible) {
                removePopup();
            }

            var p = $(this);
            var offset = p.offset();

            if (e.clientX > offset.left + p.width() - 16) {
                $("body").prepend("<iframe id='favsync_popup' width='300' height='200' scrolling='no' frameborder='0' style='position:absolute;top:" + (offset.top + p.outerHeight()) + "px;left:" + offset.left + "px;z-index: 190000' src='favsync://popup/popup.html?domain=" + domain + "'></iframe>");
                isPopupVisible = true;
                e.stopPropagation();
            }
        });
}

function removePopup() {
    $("#favsync_popup").remove();
    isPopupVisible = false;
}

function addPassGenPopup(pass) {
    pass.attr("style", "background-image:url(favsync://img/plus.png);background-attachment:scroll;background-position: 100% 50%;background-repeat: no-repeat no-repeat").
        click(function (e) {
            if (isPopupVisible) {
                removePassGenPopup();
            }

            var p = $(this);
            var offset = p.offset();

            if (e.clientX > offset.left + p.width() - 16) {
                $("body").prepend("<iframe id='favsync_pass_gen_popup' width='300' height='230' scrolling='no' frameborder='0' style='position:absolute;top:" + (offset.top + p.outerHeight()) + "px;left:" + offset.left + "px;z-index: 190000' src='favsync://popup/passgen.html'></iframe>");
                isPopupVisible = true;
                e.stopPropagation();
            }
        });
}


function removePassGenPopup() {
    $("#favsync_pass_gen_popup").remove();
    isPopupVisible = false;
}

function getLoginForm() {
    var forms = $("form");
    console.log("getLoginForm forms: " + forms.length);

    for (var i = 0; i < forms.length; i++) {
        var form = forms[i];
        var pass = $("input[type='password']", form);
        if (pass.length == 0) {
            pass = $("input#password", form);
        }
        console.log("pass.length: " + pass.length);
        if (pass.length > 0) {
            var login = $("input[type='text']", form);

            console.log("login.length: " + login.length);
            if (login.length == 0) {
                login = $("input:not([type])", form);
            }
            if (login.length == 0) {
                login = $("input[type='email']", form);
            }

            if (login.length > 0 && getVisibleInputs(form).length <= MAX_INPUTS_FOR_LOGIN) {
                return {
                    login: login,
                    pass: pass,
                    form: $(form)
                };
            }
        }
    }

    return null;
}

function getSignUpForm() {
    var forms = $("form");
    console.log("getSignUpForm forms: " + forms.length);

    for (var i = 0; i < forms.length; i++) {
        var form = forms[i];
        var pass = $("input[type='password']", form);

        if (pass.length >= 2 || (pass.length == 1 && getVisibleInputs(form).length > MAX_INPUTS_FOR_LOGIN)) {
            var login = $("input[type='text']", form);
            if (login.length == 0) {
                login = $("input:not([type])", form);
            }

            if (login.length > 0) {
                return {
                    pass: pass,
                    form: $(form)
                };
            }
        }
    }

    return null;
}

function getVisibleInputs(form) {
    return $("input:not([type=hidden])", form).filter(":visible");
}

function fireEvt(type, element) {
    var customEvent;
    var type = type;
    var bubbles = true;
    var cancelable = true;
    var view = window;
    var ctrlKey = false;
    var altKey = false;
    var shiftKey = false;
    var metaKey = false;
    var keyCode = 68;
    var charCode = 68;

    try {

        //try to create key event
        customEvent = document.createEvent("KeyEvents");

        /*
         * Interesting problem: Firefox implemented a non-standard
         * version of initKeyEvent() based on DOM Level 2 specs.
         * Key event was removed from DOM Level 2 and re-introduced
         * in DOM Level 3 with a different interface. Firefox is the
         * only browser with any implementation of Key Events, so for
         * now, assume it's Firefox if the above line doesn't error.
         */
        //TODO: Decipher between Firefox's implementation and a correct one.
        customEvent.initKeyEvent(type, bubbles, cancelable, view, ctrlKey,
            altKey, shiftKey, metaKey, keyCode, charCode);

    } catch (ex /*:Error*/) {

        /*
         * If it got here, that means key events aren't officially supported.
         * Safari/WebKit is a real problem now. WebKit 522 won't let you
         * set keyCode, charCode, or other properties if you use a
         * UIEvent, so we first must try to create a generic event. The
         * fun part is that this will throw an error on Safari 2.x. The
         * end result is that we need another try...catch statement just to
         * deal with this mess.
         */
        try {

            //try to create generic event - will fail in Safari 2.x
            customEvent = document.createEvent("Events");

        } catch (uierror /*:Error*/) {

            //the above failed, so create a UIEvent for Safari 2.x
            customEvent = document.createEvent("UIEvents");

        } finally {

            customEvent.initEvent(type, bubbles, cancelable);

            //initialize
            customEvent.view = view;
            customEvent.altKey = altKey;
            customEvent.ctrlKey = ctrlKey;
            customEvent.shiftKey = shiftKey;
            customEvent.metaKey = metaKey;
            customEvent.keyCode = keyCode;
            customEvent.charCode = charCode;

        }

    }

    //fire the event
    element.focus();
    element.dispatchEvent(customEvent);
}
