var URL_LOGINS = "http://localhost/plugin/getLogins";
// 67f5bcd089dbd58e9e2f5fa3e29b027a

var domain2li = {};
var domain2frames = {};

function init() {
    chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
        if (request.name == "get_login_info") {
            var frames = domain2frames[request.domain];
            if (!frames) {
                frames = [];
            }

            frames.push({url: request.url, lf: request.lf});

            domain2frames[request.domain] = frames;

            getLoginInfo(request.url, request.domain, request.src, sendResponse);
        } else if (request.name == "insert_lp" ||
            request.name == "use_pass" ||
            request.name == "cancel_use_pass") {
            chrome.tabs.sendMessage(sender.tab.id, request);
        } else if (request.name == "save_hash_and_last_domain") {
            var li = domain2li[request.domain];
            if (li && li.logins.length > 0) {
                var now = new Date().getTime();
                if (li.last_login_time && li.last_hash == request.hash && now - li.last_login_time < 60*1000) {
                    return;
                }

                li.last_hash = request.hash;
                li.last_login_time = now;
            }

            sendResponse({});
        } else if (request.name == "update_login_time") {
            delete domain2frames[request.domain];

            var li = domain2li[request.domain];
            if (li) {
                // li.last_hash is already updated.
                li.last_login_time = new Date().getTime();
            }
        } else if (request.name == "get_base_path") {
            sendResponse({baseURL: chrome.extension.getURL("")});
        } else if (request.name == "save_prop") {
            localStorage[request.prop_name] = request.prop_val;
        } else if (request.name == "get_prop") {
            sendResponse({prop_val: localStorage[request.prop_name]});
        }
    });
}

function wasSuccessfullyLoggedIn(url, domain) {
    var frames = domain2frames[domain];

    if (!frames) {
        return false;
    }

    for (var i = 0; i < frames.length; i++) {
        var f = frames[i];

        if (f.url == url && !f.lf) {
            return false;
        }
    }

    return true;
}

function getLoginInfo(url, domain, src, callback) {
    if (src == "popup") {
        var li = domain2li[domain];
        if (li) {
            callback(li);
        }
        return;
    }

    $.post(URL_LOGINS, {code: "67f5bcd089dbd58e9e2f5fa3e29b027a", link: url}, function (r) {
        if ($.trim(r).length == 0) {
            return;
        }
        r = JSON.parse(r);

        var result = {
            last_login_time: null,
            last_hash: null,
            logins: r.li.lp.map(function (lp) {
                    var l = lp.username;
                    var a = lp.action;
                    var p = decode(lp.password);
                    return {username: l, password: p, action : a, hash: md5(l + p)};
                }
            ),
            baseURL: chrome.extension.getURL("")
        };

        var oldLI = domain2li[domain];
        if (oldLI && result.logins.length > 0) {
            if (oldLI.last_hash != result.logins[0].hash) {
                result.last_login_time = null;
            } else {
                result.last_hash = oldLI.last_hash;
                result.last_login_time = oldLI.last_login_time;
            }
        }

        result.success = wasSuccessfullyLoggedIn(url, domain);

        callback(result);

        domain2li[domain] = result;
    });
}

$(document).ready(function () {
    $.ajaxSetup({cache: false});
    init();
});
