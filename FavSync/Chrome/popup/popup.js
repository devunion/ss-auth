$(document).ready(function(){
    var domain = document.location.toString().split("domain=")[1];

    chrome.extension.sendRequest({name: "get_login_info", src : "popup", domain: domain}, function (msg) {

        for (var i=0; i < msg.logins.length; i++) {
            var lp = msg.logins[i];
            $("#container").append(getLPBox(lp));
        }
    });
//    $("body").text(domain);
});

function getLPBox(lp) {
    return $("<div class='lp-box' login='" + lp.username + "'>Login: " + lp.username + "</div>").click(function(){
        var login = $(this).attr("login");
        $(this).removeAttr("login");

        chrome.extension.sendRequest({name: "insert_lp", username : login})
    });
}
