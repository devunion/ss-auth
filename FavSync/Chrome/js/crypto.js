var ENCRYPTION_KEY = '54SEF--dsa3!';

function decode(string) {
    var hash = "";
    if ($.trim(string).length != 0) {
        var strings = string.split("-");
        string = strings[0];

        var key = ENCRYPTION_KEY;
        key = sha1(key);

        var keyLen = key.length;
        var j = 0;
        for (var i = 0; i < string.length; i += 2) {
            var ordStr = hexdec(base_convert(strrev(substr(string, i, 2)), 36, 16));
            if (j == keyLen) {
                j = 0;
            }
            var ordKey = ord(substr(key, j, 1));
            j++;
            hash += chr(ordStr - ordKey);
        }

        //hash check
        if (md5(hash) != strings[1])
            return "-";
    }
    return hash;
}
